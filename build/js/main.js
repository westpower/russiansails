$(document).ready(function(){
    $('.sponsors-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false
    });
});

$(document).ready(function(){
    $('.main-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false
    });
});

$(document).ready(function(){
    $('.crew-slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        swipe: true
    });    
    $('.crew-slider').on('beforeChange', function(event, slick, direction){
        changeSizeSlides(slick);
    });
    $('.crew-slider').slick('slickNext');

    
    function changeSizeSlides(slick) {
        var activeSlides = slick['$slides'].filter('.slick-active');
        activeSlides.each(function(i, el) {
            el.classList.remove('large');
            el.classList.remove('medium');
        });
        activeSlides[3].classList.add('large');
        activeSlides[2].classList.add('medium');
        activeSlides[4].classList.add('medium');
    }
});

$(document).ready(function(){
    $('.counter').counterUp({
        delay: 1,
        time: 500
    });

    $('.counter').addClass('show');
});